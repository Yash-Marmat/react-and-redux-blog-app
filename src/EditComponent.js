import React, { Component } from "react";
import { connect } from "react-redux";

class EditComponent extends Component {
  // handles form updates
  handleEdit = (e) => {
    e.preventDefault();
    const newTitle = this.getTitle.value;
    const newMessage = this.getMessage.value;
    const data = {
      newTitle,
      newMessage,
    };
    //console.log(data);
    // below, storing our above generated data in redux state (thanks to connect)
    this.props.dispatch({
      type: "UPDATE",
      id: this.props.post.id,
      data: data,
    });
    this.getTitle.value = "";
    this.getMessage.value = "";
  };

  render() {
    return (
      <div>
        <h2>Update Post</h2>
        <form onSubmit={this.handleEdit}>
          <input
            type="text"
            ref={(input) => (this.getTitle = input)}
            defaultValue={this.props.post.title}
            placeholder="title here"
            required
          />
          <br />
          <br />
          <textarea
            required
            ref={(input) => (this.getMessage = input)}
            defaultValue={this.props.post.message}
            placeholder="your content here"
            rows="5"
            cols="28"
          />
          <br />
          <button>Update</button>
        </form>
      </div>
    );
  }
}

export default connect()(EditComponent);
