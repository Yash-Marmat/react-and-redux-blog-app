import React, { Component } from "react";
import { connect } from "react-redux";

class PostForm extends Component {
  // handles form
  handleSubmit = (e) => {
    e.preventDefault();
    const title = this.getTitle.value;
    const message = this.getMessage.value;
    const data = {
      id: new Date(),
      title,
      message,
      editing: false,
    };
    //console.log(data);
    // below, storing our above generated data in redux state (thanks to connect)
    this.props.dispatch({
      type: "ADD_POST",
      data,
    });
    this.getTitle.value = "";
    this.getMessage.value = "";
  };

  render() {
    return (
      <div>
        <h2>Create Post</h2>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            ref={(input) => (this.getTitle = input)}
            placeholder="title here"
            required
          />
          <br />
          <br />
          <textarea
            required
            ref={(input) => (this.getMessage = input)}
            placeholder="your content here"
            rows="5"
            cols="28"
          />
          <br />
          <button>Post</button>
        </form>
      </div>
    );
  }
}

export default connect()(PostForm);
