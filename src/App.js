import React from "react";
import "./App.css";
import PostForm from "./PostForm";
import AllPosts from "./AllPosts";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <PostForm />
        <br />
        <AllPosts />
      </div>
    );
  }
}

export default App;
