import React, { Component } from "react";
import { connect } from "react-redux";
import Post from "./Post";
import EditComponent from "./EditComponent";

// The key (posts) that we use in mapStateToProps will be available
// to us as props inside the react component.
const mapStateToProps = (state) => {
  return {
    posts: state,
  };
};

class AllPosts extends Component {
  render() {
    return (
      <div>
        <h3>All Posts</h3>
        {this.props.posts.map((post) => (
          <div key={post.id}>
            {post.editing ? (
              <EditComponent post={post} key={post.id} />
            ) : (
              <Post key={post.id} post={post} />
            )}
          </div>
        ))}
      </div>
    );
  }
}

export default connect(mapStateToProps)(AllPosts);
